const express = require('express')
const { json, urlencoded } = require('body-parser')
const app = express()

app.use(json())
app.use(urlencoded({ extended: true }))

const userRouter = require('./routers/user')
const courseRouter = require('./routers/course')
const pembelianRouter = require('./routers/pembelian')

app.get('/', (req, res) => res.redirect('https://mercusuar.uzone.id/'))

app.use("/v1", userRouter)
app.use("/v1", courseRouter)
app.use("/v1", pembelianRouter)

app.listen(3000, () => console.log('Be Connect'))