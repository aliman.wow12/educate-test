'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tb_course extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.tb_pembelian, { as: "course_pembelian", foreignKey: "id_course" })
    }
  }
  tb_course.init({
    name: DataTypes.STRING,
    desc: DataTypes.STRING,
    price: DataTypes.INTEGER,
    file_video: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'tb_course',
    timestamps: true,
    freezeTableName: true,
    paranoid: true
  });
  return tb_course;
};