'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tb_pembelian extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.tb_user, { as: "user_pembelian", targetKey: "id", foreignKey: "id_user" })
      this.belongsTo(models.tb_course, { as: "course_pembelian", targetKey: "id", foreignKey: "id_course" })
    }
  }
  tb_pembelian.init({
    id_user: DataTypes.INTEGER,
    id_course: DataTypes.INTEGER,
    total_payment: DataTypes.INTEGER,
    desc: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'tb_pembelian',
    timestamps: true,
    freezeTableName: true,
    paranoid: true
  });
  return tb_pembelian;
};