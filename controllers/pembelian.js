const { tb_pembelian, tb_user, tb_course } = require('../models')
const { Op } = require('sequelize')
const restem = require('../helpers/response_template')
const checkatr = require('../helpers/check_attributes')
const fltobj = require('../helpers/filter_object')

const exclude = ['deletedAt']

const include = [
    { model: tb_user, as : 'user_pembelian', where: { deletedAt : null }, attributes : { exclude : [  'deletedAt', 'password' ] }},
    { model: tb_course, as : 'course_pembelian', where: { deletedAt : null }, attributes : { exclude : [  'deletedAt' ] }}
]

module.exports = {
    getData: async (req, res) => {
        try {
            const query = req.query
            const offset = query.start ? parseInt(query.start) : null
            const limit = query.length ? parseInt(query.length) : null
            const order = query.order ? query.order == 'asc' ? 'ASC' : query.order == 'desc' ? 'DESC' : 'DESC' : 'DESC'
            const createdAt = query.createdAt ? query.createdAt.includes(',')
                ? { createdAt: { [Op.between]: [query.createdAt.split(',')[0], query.createdAt.split(',')[1]] } }
                : { createdAt: query.createdAt } : {}
            const updatedAt = query.updatedAt ? query.updatedAt.includes(',')
                ? { updatedAt: { [Op.between]: [query.updatedAt.split(',')[0], query.updatedAt.split(',')[1]] } }
                : { updatedAt: query.updatedAt } : {}
            const idCourse = query.id_course ? { 'id_course': query.id_course  } : {}
            const idUser = query.id_user ? { 'id_user': query.id_user  } : {}
            const where = { ...idUser, ...idCourse, ...createdAt, ...updatedAt, deletedAt: null }

            if (query.id) {
                const singleData = await tb_pembelian.findOne({ where: { id: query.id, deletedAt: null }, attributes: { exclude }, include })
                return res.status(200).json(restem.get_single_success(singleData))
            }

            const listData = await tb_pembelian.findAll({ where, offset, limit, order: [['id', order]], attributes: { exclude }, include })
            const total = await tb_pembelian.count({ where : { deletedAt: null } }) || 0
            return res.status(200).json(restem.get_success(total, listData.length, listData))

        } catch (error) {
            return res.status(400).json(restem.get_failed(400, 'Terjadi kendala sistem!', error.toString()))
        }
    },
    sendData: async (req, res) => {
        try {
            const body = req.body
            let hasil = []
            if (Array.isArray(body)) {
                for (let payload of body) {
                    if (await checkatr(tb_pembelian, payload) == false) { return res.status(400).json(restem.pos_failed(400, 'Formulir salah!')) }

                    if (payload.id) {
                        const upResult = await tb_pembelian.update(fltobj(payload, ([key, value]) => key !== "id"), { where: { id: payload.id } })
                        if (upResult.length != 0) {
                            const resUpdate = await tb_pembelian.findOne({ where: { id: payload.id, deletedAt: null }, attributes: { exclude }, include })
                            hasil.push(restem.pos_success('Pembelian berhasil diperbarui!', resUpdate))
                        }
                    } else {
                        if (!payload.id_user) { return res.status(400).json(restem.pos_failed(400, 'id_user tidak diketahui!', null)) }
                        if (!payload.id_course) { return res.status(400).json(restem.pos_failed(400, 'id_course tidak diketahui!', null)) }

                        let crResult = await tb_pembelian.create(payload)
                        hasil.push(restem.pos_success('Pembelian berhasil dibuat!', fltobj(crResult.dataValues, ([key, value]) => key !== "password" && key !== "deletedAt")))
                    }

                    if (hasil.length == body.length) {
                        return res.status(200).json(hasil)
                    }
                }
            } else {
                return res.status(400).json(restem.pos_failed(400, 'Format body salah!', null))
            }
        } catch (error) {
            return res.status(400).json(restem.pos_failed(400, 'Terjadi kendala sistem!', error.toString()))
        }
    },
    deletes: async (req, res) => {
        try {
            const body = req.body
            let hasil = []
            if (Array.isArray(body)) {
                for (const payload of body) {
                    const delData = await tb_pembelian.destroy({ where: { id: payload } })
                    if (delData == 0) {
                        return res.status(400).json(restem.pos_failed(400, `Pembelian dengan id ${payload} tidak ada di database!`, null))
                    } else {
                        hasil.push(`Pembelian dengan id ${payload} berhasil dihapus!`)
                    }


                    if (hasil.length == body.length) {
                        return res.status(200).json(restem.pos_success('Berhasil menghapus pembelian!', hasil))
                    }
                }
            } else {
                return res.status(400).json(restem.pos_failed(400, 'Format body salah!', null))
            }
        } catch (error) {
            return res.status(400).json(restem.pos_failed(400, 'Terjadi kendala sistem!', error.toString()))
        }

    }
}