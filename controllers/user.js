const { tb_user } = require('../models')
const { Op } = require('sequelize')
const restem = require('./../helpers/response_template')
const checkatr = require('./../helpers/check_attributes')
const fltobj = require('./../helpers/filter_object')
const bcrypt = require('bcrypt')

const exclude = ['password', 'deletedAt']

module.exports = {
    getData: async (req, res) => {
        try {
            const query = req.query
            const offset = query.start ? parseInt(query.start) : null
            const limit = query.length ? parseInt(query.length) : null
            const order = query.order ? query.order == 'asc' ? 'ASC' : query.order == 'desc' ? 'DESC' : 'DESC' : 'DESC'
            const createdAt = query.createdAt ? query.createdAt.includes(',')
                ? { createdAt: { [Op.between]: [query.createdAt.split(',')[0], query.createdAt.split(',')[1]] } }
                : { createdAt: query.createdAt } : {}
            const updatedAt = query.updatedAt ? query.updatedAt.includes(',')
                ? { updatedAt: { [Op.between]: [query.updatedAt.split(',')[0], query.updatedAt.split(',')[1]] } }
                : { updatedAt: query.updatedAt } : {}
            const name = query.name ? { 'name': { [Op.like]: `%${query.name}%` } } : {}
            const email = query.email ? { 'email': { [Op.like]: `%${query.email}%` } } : {}
            const where = { ...name, ...email, ...createdAt, ...updatedAt, deletedAt: null }

            if (query.id) {
                const singleData = await tb_user.findOne({ where: { id: query.id, deletedAt: null }, attributes: { exclude } })
                return res.status(200).json(restem.get_single_success(singleData))
            }

            const listData = await tb_user.findAll({ where, offset, limit, order: [['id', order]], attributes: { exclude } })
            const total = await tb_user.count({ where : { deletedAt: null } }) || 0
            return res.status(200).json(restem.get_success(total, listData.length, listData))

        } catch (error) {
            return res.status(400).json(restem.get_failed(400, 'Terjadi kendala sistem!', error.toString()))
        }
    },
    sendData: async (req, res) => {
        try {
            const body = req.body
            let hasil = []
            if (Array.isArray(body)) {
                for (let payload of body) {
                    if (await checkatr(tb_user, payload) == false) { return res.status(400).json(restem.pos_failed(400, 'Formulir salah!')) }

                    if (payload.email) {
                        const checkEmail = await tb_user.count({ where: { email: payload.email, deletedAt: null }})
                        if (checkEmail !== 0) {
                            return res.status(400).json(restem.pos_failed(400, `Email ${payload.email} ini sudah ada!`, null))
                        }
                    }

                    if (payload.password) {
                        const salt = bcrypt.genSaltSync(10);
                        const hash = bcrypt.hashSync(payload.password, salt);
                        payload.password = hash
                    }

                    if (payload.id) {
                        const upResult = await tb_user.update(fltobj(payload, ([key, value]) => key !== "id"), { where: { id: payload.id } })
                        if (upResult.length != 0) {
                            const resUpdate = await tb_user.findOne({ where: { id: payload.id, deletedAt: null }, attributes: { exclude } })
                            hasil.push(restem.pos_success('Data user berhasil diperbarui!', resUpdate))
                        }
                    } else {
                        if (!payload.password) { return res.status(400).json(restem.pos_failed(400, 'Password tidak diketahui!', null)) }
                        if (!payload.email) { return res.status(400).json(restem.pos_failed(400, 'Email tidak diketahui!', null)) }
                        if (!payload.name) { return res.status(400).json(restem.pos_failed(400, 'Name tidak diketahui!', null)) }

                        let crResult = await tb_user.create(payload)
                        hasil.push(restem.pos_success('Data user berhasil dibuat!', fltobj(crResult.dataValues, ([key, value]) => key !== "password" && key !== "deletedAt")))
                    }

                    if (hasil.length == body.length) {
                        return res.status(200).json(hasil)
                    }
                }
            } else {
                return res.status(400).json(restem.pos_failed(400, 'Format body salah!', null))
            }
        } catch (error) {
            return res.status(400).json(restem.pos_failed(400, 'Terjadi kendala sistem!', error.toString()))
        }
    },
    deletes: async (req, res) => {
        try {
            const body = req.body
            let hasil = []
            if (Array.isArray(body)) {
                for (const payload of body) {
                    const delData = await tb_user.destroy({ where: { id: payload } })
                    if (delData == 0) {
                        return res.status(400).json(restem.pos_failed(400, `Data user dengan id ${payload} tidak ada di database!`, null))
                    } else {
                        hasil.push(`Data user dengan id ${payload} berhasil dihapus!`)
                    }


                    if (hasil.length == body.length) {
                        return res.status(200).json(restem.pos_success('Berhasil menghapus data user!', hasil))
                    }
                }
            } else {
                return res.status(400).json(restem.pos_failed(400, 'Format body salah!', null))
            }
        } catch (error) {
            return res.status(400).json(restem.pos_failed(400, 'Terjadi kendala sistem!', error.toString()))
        }

    }
}