module.exports = {
    pos_success : (message, data) => { return { status: 200, message, data } },
    pos_failed : (status, message, error) => { return { status: status ?? 400, message : message ?? 'Data gagal terkirim!', error } },
    pos_result : (success, error) => { return { success, error } },
    get_success : (total, filtered, data) => { return { status: 200, total, filtered, data } },
    get_single_success : (data) => { return { status: 200, data } },
    get_failed : (status, message, error) => { return { status: status ?? 400, message : message ?? 'Data gagal diambil!', error } },
}