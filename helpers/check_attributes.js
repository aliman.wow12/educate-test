module.exports = async (Model, val) => {
    const keys = Object.keys(val)
    const modelKeys = Object.keys(await Model.rawAttributes)
    var checkData = keys.every(val => {
        return modelKeys.includes(val)
    })
    return checkData
}