module.exports = (obj, fun) => {
    let fltobj = Object.entries(obj)
    let fltarray = fltobj.filter(fun);
    return Object.fromEntries(fltarray)
}