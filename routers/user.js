const { Router } = require('express')
const { deletes, getData, sendData } = require('./../controllers/user')
const route = Router()

route.get('/user', getData)
route.post('/user/send', sendData)
route.post('/user/deletes', deletes)

module.exports = route