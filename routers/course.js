const { Router } = require('express')
const { deletes, getData, sendData } = require('../controllers/course')
const route = Router()

route.get('/course', getData)
route.post('/course/send', sendData)
route.post('/course/deletes', deletes)

module.exports = route