const { Router } = require('express')
const { deletes, getData, sendData } = require('../controllers/pembelian')
const route = Router()

route.get('/pembelian', getData)
route.post('/pembelian/send', sendData)
route.post('/pembelian/deletes', deletes)

module.exports = route